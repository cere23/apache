class mysql {
  $password = "ciao"
  package { "mysql-client": ensure => installed }
  package { "mysql-server": ensure => installed }

  exec { "Set MySQL server root password":
    subscribe => [ Package["MySQL-server"], Package["MySQL-client"] ],
    refreshonly => true,
    unless => "mysqladmin -uroot -p$password status",
    path => "/bin:/usr/bin",
    command => "mysqladmin -uroot password $password",
  }

service { 'mysql':
        ensure  => running,
        enable  => true,
    }


}
